use failure;
use headless_chrome::Browser;
use pulldown_cmark::{html, Options, Parser};
use std::fs;
use std::fs::File;
use std::io::prelude::*;

use std::path::{Path, PathBuf};
use structopt::StructOpt;

use std::sync::mpsc::channel;

use notify::{self, DebouncedEvent};
use notify::{RecursiveMode, Watcher};
use std::time::Duration;

/// Converts md file to html, accepts a css file as an option.
#[derive(StructOpt, Debug)]
#[structopt(name = "rumdf")]
struct Opt {
    /// Markdown file to read
    #[structopt(name = "input", parse(from_os_str))]
    input: PathBuf,

    /// Where to write the pdf
    #[structopt(name = "output", parse(from_os_str))]
    output: Option<PathBuf>,

    /// Css file for styling
    /// Defaults to ~/.config/rumdf/default.css
    #[structopt(long)]
    css: Option<PathBuf>,

    /// Watch mode (Only md for now, save md to reload css)
    #[structopt(name = "watch", long)]
    watch: bool,
}

/**
 * TODO:
 * - [ ] Write temp html to /tmp dir
 * - [ ] Distribute default css
 */

fn main() -> Result<(), failure::Error> {
    let opt = Opt::from_args();

    let css_file_path = match opt.css {
        Some(file) => file,
        None => {
            let mut dir = dirs::home_dir().expect("Could not find home directory");
            dir.push(".config/rumdf/default.css");
            dir
        }
    };

    let output_path = match opt.output {
        Some(pathbuf) => pathbuf,
        _ => {
            let mut path = opt.input.clone();
            path.set_extension("pdf");
            println!("No output filename given, defaulting to {:?}", path);
            path
        }
    };

    generate_pdf_from_md(
        opt.input.clone(),
        css_file_path.clone(),
        output_path.clone(),
    )?;

    if opt.watch {
        // Create a channel to receive the events.
        let (tx, rx) = channel();
        let mut watcher = notify::watcher(tx, Duration::from_secs(2))?;

        watcher.watch(Path::new("."), RecursiveMode::Recursive)?;

        loop {
            match rx.recv() {
                Ok(DebouncedEvent::NoticeWrite(_path)) => {
                    let start = std::time::Instant::now();
                    generate_pdf_from_md(
                        opt.input.clone(),
                        css_file_path.clone(),
                        output_path.clone(),
                    )?;
                    let end = std::time::Instant::now();
                    let ellapsed = end - start;
                    println!("Generated in: {}ms", ellapsed.as_millis());
                }
                Err(error) => println!("{}", error),
                _ => (),
            }
        }
    }

    Ok(())
}

fn generate_pdf_from_md(
    md_path: PathBuf,
    css_path: PathBuf,
    output_path: PathBuf,
) -> Result<(), failure::Error> {
    let mut md_file_content = String::new();
    {
        let mut md_file = File::open(md_path)?;
        md_file.read_to_string(&mut md_file_content)?;
    }
    // Add css link
    let os_string = fs::canonicalize(css_path)?.clone().into_os_string();
    let css_file = os_string.to_string_lossy();
    md_file_content
        .push_str(format!(r#"{}<link rel="stylesheet"  href="{}">"#, '\n', css_file).as_str());

    // Set up options and parser. Strikethroughs are not part of the CommonMark standard
    // and we therefore must enable it explicitly.
    let mut options = Options::empty();
    options.insert(Options::ENABLE_STRIKETHROUGH);
    options.insert(Options::ENABLE_TABLES);
    let parser = Parser::new_ext(&md_file_content, options);

    // Write to String buffer.
    let mut html_output: String = String::with_capacity(md_file_content.len() * 3 / 2);
    html::push_html(&mut html_output, parser);

    let path = PathBuf::from("./temp.html");
    let mut file = File::create(path.clone())?;
    file.write_all(html_output.as_bytes())?;

    let pdf_content = generate_pdf_for_html(path.clone())?;
    fs::remove_file(path)?;
    let mut pdf = File::create(output_path.clone())?;
    pdf.write_all(&pdf_content)?;

    println!("Your pdf file has been written to:"); println!("{}", fs::canonicalize(output_path)?.to_string_lossy());

    Ok(())
}

fn generate_pdf_for_html(file: PathBuf) -> Result<Vec<u8>, failure::Error> {
    let browser = Browser::default()?;

    let tab = browser.wait_for_initial_tab()?;

    // Navigate to wikipedia
    tab.navigate_to(format!("file://{}", fs::canonicalize(file)?.to_string_lossy()).as_str())?;

    let pdf = tab.print_to_pdf(None)?;

    Ok(pdf)
}
