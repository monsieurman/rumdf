# Rumdf

This project is yet another markdown to pdf converter.

It is built in Rust, uses an installed version of a chromium based browser to convert html to pdf.

It accepts a css file, defaulting to `~/.config/rumdf/default.css`.

## Installation

You can install it with cargo via

```sh
cargo install --git https://gitlab.com/monsieurman/rumdf.git
```

## Usage

Run `rumdf --help` for usage information.

```
Converts md file to html, accepts a css file as an option

USAGE:
    rumdf [FLAGS] [OPTIONS] <input> [output]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
        --watch      Watch mode (Only md for now, save md to reload css)

OPTIONS:
        --css <css>    Css file for styling Defaults to ~/.config/rumdf/default.css

ARGS:
    <input>     Markdown file to read
    <output>    Where to write the pdf
```

### Converting a markdown file, using default css

```sh
rumdf my_file.md
```

### Watch changes and recompile automatically

```sh
rumdf --watch my_file.md
```

### Specify css file

```sh
rumdf --css custom.css my_file.md
```